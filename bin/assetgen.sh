#!/usr/bin/env sh
file="lib/api/services/AssetService.ts"
script="modules/assetgen.nvim"

nvim --headless -s "$script" "$file" 2>/dev/null
prettier --write "$file" >/dev/null