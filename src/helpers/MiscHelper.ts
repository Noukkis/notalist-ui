import { Collection, Media, Entry } from '@lib/api';

const spaces = /[\s-]+/g;
const specialCharacters = /[^A-Za-z0-9_-]/g;

export function sanitize(s: string) {
  return s.replace(spaces, '-').replace(specialCharacters, '');
}

export function detailsPath(collection: Collection, media: Media) {
  return {
    name: 'details',
    params: {
      id: media.id,
      collectionName: sanitize(collection.name),
      name: sanitize(media.name),
    },
  };
}

export function icontains(a: string, b: string) {
  return a.toLocaleLowerCase().includes(b.toLocaleLowerCase());
}

export function lastEntry(media: Media): Entry {
  const entries = media.entries;
  return entries[entries.length - 1];
}

export const comparators = {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  default: (m: Media) => undefined,
  name: (m: Media) => m.name.toLowerCase(),
  started: (m: Media) => lastEntry(m)?.started,
  completed: (m: Media) => lastEntry(m)?.completed,
  start: (m: Media) => m.start,
  end: (m: Media) => m.end,
  score: (m: Media) => {
    const score = lastEntry(m)?.score;
    return score ? -score : Infinity;
  },
};
