import { defineStore } from 'pinia';
import { CollectionService, MediaService } from '@lib/api';
import type { Media, Collection } from '@lib/api';
import { sanitize, lastEntry } from '@/helpers/MiscHelper';

export const useCollectionsStore = defineStore('collections', {
  state: () => ({
    collections: new Array<Collection>(),
    medias: new Map<string, Media[]>(),
  }),

  actions: {
    async addCollection(collection: Collection): Promise<void> {
      this.collections.push(collection);
    },

    async refreshCollections(): Promise<void> {
      this.collections.length = 0;
      const collections = await CollectionService.listCollections();
      collections.forEach((c) => this.collections.push(c));
    },

    getCollection(sanitizedName?: string) {
      if (!sanitizedName) return null;
      return this.collections.find((c) => sanitize(c.name) === sanitizedName);
    },

    async refreshMedias(collection: Collection) {
      const medias = await MediaService.listMedias(collection.id);
      const statuses = collection.entryStatuses;
      if (statuses)
        medias.sort((a, b) => statuses.indexOf(lastEntry(a).status) - statuses.indexOf(lastEntry(b).status));
      this.medias.set(collection.name, medias);
    },

    addMedia(collection: Collection, media: Media) {
      const medias = this.medias.get(collection.name);
      if (medias) medias.push(media);
    },
  },
});
