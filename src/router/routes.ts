import { RouteRecordRaw } from 'vue-router';

import CollectionLayout from '@/layouts/CollectionLayout.vue';
import SettingsLayout from '@/layouts/SettingsLayout.vue';

import CollectionView from '@/pages/collection/CollectionView.vue';
import EntryAdder from '@/pages/collection/EntryAdder.vue';
import MediaDetails from '@/pages/collection/MediaDetails.vue';

import CollectionsSettings from '@/pages/settings/CollectionsSettings.vue';
import ImportSettings from '@/pages/settings/ImportSettings.vue';
import AboutSettings from '@/pages/settings/AboutSettings.vue';

import ErrorNotFound from '@/pages/ErrorNotFound.vue';

const routes: RouteRecordRaw[] = [
  { name: 'root', path: '/', redirect: { name: 'collection-home' } },

  {
    name: 'collection-home',
    path: '/collection/:collectionName?',
    component: CollectionLayout,
    props: true,
    children: [
      { name: 'collection', path: '', component: CollectionView, props: true },
      { name: 'addEntry', path: 'add', component: EntryAdder, props: true },
      { name: 'details', path: 'details/:id/:name?', component: MediaDetails, props: true },
    ],
  },

  {
    name: 'settings',
    path: '/settings',
    component: SettingsLayout,
    children: [
      { name: 'settings-collections', path: 'collections', component: CollectionsSettings },
      { name: 'settings-import', path: 'import', component: ImportSettings },
      { name: 'settings-about', path: 'about', component: AboutSettings },
    ],
    props: true,
  },

  {
    name: '404',
    path: '/404',
    component: ErrorNotFound,
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    redirect: { name: '404' },
  },
];

export default routes;
